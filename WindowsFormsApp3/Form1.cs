﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp3
{
    public partial class Form1 : Form
    {
        float a, b, c, d, m;
        int count;
        bool znak = true;
        string oper;
        double kor;

        public Form1()
        {
            InitializeComponent();
        }

        private void calculate()
        {
            switch (count)
            {
                case 1:
                    b = a + float.Parse(textBox1.Text);
                    textBox1.Text = b.ToString();
                    break;
                case 2:
                    b = a - float.Parse(textBox1.Text);
                    textBox1.Text = b.ToString();
                    break;
                case 3:
                    b = a * float.Parse(textBox1.Text);
                    textBox1.Text = b.ToString();
                    break;
                case 4:
                    b = a / float.Parse(textBox1.Text);
                    textBox1.Text = b.ToString();
                    break;
                case 5:
                    b = (a/100) * c;
                    if (oper == "+") d = a + b;
                    else
                        if (oper == "-") d = a - b;
                    else
                        if (oper == "*") d = a * b;
                    else
                        if (oper == "/") d = a / b;
                        textBox1.Text = d.ToString();
                    break;
                default:
                    break;
            }

        }

        private void button23_Click(object sender, EventArgs e)
        {
            textBox1.Text = textBox1.Text + 0;
        }

        private void button17_Click(object sender, EventArgs e)
        {
            textBox1.Text = textBox1.Text + 1;
        }

        private void button18_Click(object sender, EventArgs e)
        {
            textBox1.Text = textBox1.Text + 2;
        }

        private void button19_Click(object sender, EventArgs e)
        {
            textBox1.Text = textBox1.Text + 3;
        }

        private void button11_Click(object sender, EventArgs e)
        {
            textBox1.Text = textBox1.Text + 4;
        }

        private void button12_Click(object sender, EventArgs e)
        {
            textBox1.Text = textBox1.Text + 5;
        }

        private void button13_Click(object sender, EventArgs e)
        {
            textBox1.Text = textBox1.Text + 6;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            textBox1.Text = textBox1.Text + 7;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            textBox1.Text = textBox1.Text + 8;
        }

        private void button7_Click(object sender, EventArgs e)
        {
            textBox1.Text = textBox1.Text + 9;
        }

        private void button26_Click(object sender, EventArgs e)
        {
            a = float.Parse(textBox1.Text);
            textBox1.Clear();
            count = 1;
            test1.Text = a.ToString() + "+";
            znak = true;
            oper = "+";
        }

        private void button20_Click(object sender, EventArgs e)
        {
            a = float.Parse(textBox1.Text);
            textBox1.Clear();
            count = 2;
            test1.Text = a.ToString() + "-";
            znak = true;
            oper = "-";
        }

        private void button14_Click(object sender, EventArgs e)
        {
            a = float.Parse(textBox1.Text);
            textBox1.Clear();
            count = 3;
            test1.Text = a.ToString() + "*";
            znak = true;
            oper = "*";
        }

        private void button8_Click(object sender, EventArgs e)
        {
            a = float.Parse(textBox1.Text);
            textBox1.Clear();
            count = 4;
            test1.Text = a.ToString() + "/";
            znak = true;
            oper = "/";
        }

        private void button27_Click(object sender, EventArgs e)
        {
            calculate();
            test1.Text = "";
        }

        private void button3_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
            test1.Text = "";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int lenght = textBox1.Text.Length - 1;
            string text = textBox1.Text;
            textBox1.Clear();
            for (int i = 0; i < lenght; i++)
            {
                textBox1.Text = textBox1.Text + text[i];
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
        }

        private void button21_Click(object sender, EventArgs e)
        {
            a = float.Parse(textBox1.Text);
            b = 1 / a;
            textBox1.Text = b.ToString(); 
            test1.Text = "1/" + a.ToString();
            znak = true;
            oper = "/";
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.ControlBox = false;

        }

        private void button9_Click(object sender, EventArgs e)
        {
            a = float.Parse(textBox1.Text);
            kor = Math.Sqrt(a);
            textBox1.Text = kor.ToString();
            test1.Text = "sqrt(" + a.ToString() + ")";
            znak = true;

        }

        private void копироватьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(textBox1.Text);
        }

        private void вставитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            textBox1.Paste();
        }

        private void завершитьРаботуToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void свернутьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void button10_Click(object sender, EventArgs e)
        {
            if (memory1.Text != "") textBox1.Text = m.ToString();

        }

        private void button4_Click(object sender, EventArgs e)
        {
            memory1.Text = "";
        }

        private void button22_Click(object sender, EventArgs e)
        {
            if (memory1.Text != "")
            {
                b = m + float.Parse(textBox1.Text);
                textBox1.Text = b.ToString();
                test1.Text = "";
                znak = true;
            }
        }

        private void button28_Click(object sender, EventArgs e)
        {
            if (memory1.Text != "")
            {
                b = m - float.Parse(textBox1.Text);
                textBox1.Text = b.ToString();
                test1.Text = "";
                znak = true;
            }
        }

        private void справкаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string message = "Автор: Харченко Екатерина Викторовна";
            string title = "Справка";
            MessageBox.Show(message, title);
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            char number = e.KeyChar;
            if ((e.KeyChar <= 47 || e.KeyChar >= 58) && number != 8 && (e.KeyChar <= 39 || e.KeyChar >= 46) && number != 47 && number != 61) //калькулятор
            {
                e.Handled = true;
            }
        }

        private void button16_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "")
            {
                m = float.Parse(textBox1.Text);
                memory1.Text = "M";
            }
        }

        private void button24_Click(object sender, EventArgs e)
        {
            if (znak == true)
            {
                textBox1.Text = "-" + textBox1.Text;
                znak = false;
            }
            else if (znak == false)
            {
                textBox1.Text = textBox1.Text.Replace("-", "");
                znak = true;
            }
        }

        private void button15_Click(object sender, EventArgs e)
        {
            c = float.Parse(textBox1.Text);
            count = 5;
            test1.Text = test1.Text + textBox1.Text + "%";
            calculate();
            znak = true;
        }

        private void button25_Click(object sender, EventArgs e)
        {
            textBox1.Text = textBox1.Text + ",";
        }
    }
}
